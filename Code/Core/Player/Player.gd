extends CharacterBody3D
class_name Player

const BLOCK = preload("res://Core/Block/Block.tscn")

@onready var rig = $Rig

@export var mouse_look_multiplier: float = 0.5

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var speed = 7.0 # m / sec.
var jump_velocity = 6.5
var kill_floor = -100


func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CONFINED_HIDDEN


func _physics_process(delta):
	if position.y < kill_floor:
		position = Vector3.ZERO
		position.y += 2
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	# Handle Jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = jump_velocity
	
	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)
	
	move_and_slide()
	
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
	
	if Input.is_action_just_pressed("drop_block"):
		var block = BLOCK.instantiate()
		block.position = position
		block.position -= global_transform.basis.z * 1.5 # TODO: Toss a block in front.
		block.position.y += 3
		get_parent().add_child(block)
	
	if Input.is_action_just_pressed("toss_block"):
		var block = BLOCK.instantiate()
		block.position = position
		block.position -= global_transform.basis.z * 1.5 # TODO: Toss a block in front.
		block.position.y += 3
		block.apply_central_impulse((global_transform.basis.z - global_transform.basis.y) * -5)
		get_parent().add_child(block)


func _input(event):
	if event is InputEventMouseMotion:
		rotation_degrees.y += -event.relative.x * mouse_look_multiplier
		rig.rotation_degrees.x += -event.relative.y * mouse_look_multiplier
		rig.rotation_degrees.x = clamp(rig.rotation_degrees.x, -70, 17.5)
